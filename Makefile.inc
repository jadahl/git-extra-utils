srcdir = .

prefix ?= ${HOME}/.local
bindir ?= ${HOME}/.local/bin
sbindir ?= ${HOME}/.local/sbin
libexecdir ?= ${HOME}/.local/libexec
datarootdir ?= ${HOME}/.local/share
datadir ?= ${HOME}/.local/share
sysconfdir ?= ${HOME}/.local/etc
libdir ?= ${HOME}/.local/lib
mandir ?= ${HOME}/.local/share/man
enable_documentation ?= yes
