include Makefile.inc
VPATH=$(srcdir)

available_docs = git-reviewed-by.html git-reviewed-by.1 git-reviewed-by.xml git-archive-branch.html git-archive-branch.1 git-archive-branch.xml

ifeq ($(enable_documentation),yes)
docs = $(available_docs)
else
docs =
endif

all: $(docs)

%.xml: %.txt
	asciidoc -f $(srcdir)/asciidoc.conf -d manpage -b docbook -o $@ $<

%.html: %.txt
	asciidoc -f $(srcdir)/asciidoc.conf -d manpage -o $@ $<

%.1: %.xml
	xmlto man $<

clean:
	rm -f $(available_docs)

install: install-bin $(if $(findstring yes,$(enable_documentation)),install-doc)

install-bin:
	mkdir -p $(DESTDIR)$(bindir)
	install -m 0755 $(srcdir)/git-reviewed-by $(DESTDIR)$(bindir)
	install -m 0755 $(srcdir)/git-archive-branch $(DESTDIR)$(bindir)

install-doc: git-reviewed-by.1
	mkdir -p $(DESTDIR)$(mandir)/man1
	install -m 0644 git-reviewed-by.1 $(DESTDIR)$(mandir)/man1
	install -m 0644 git-archive-branch.1 $(DESTDIR)$(mandir)/man1
